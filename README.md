# Halo112

![aplikacija](./slika_aplikacije.png)

## Pokretanje

Za pokretanje na lokalnom računalu je potrebna Python inačica >= 3.9.

1. Stvoriti Python venv (virtual environment) s naredbom:
```python
python3 -m venv .venv
```
2. Aktivirati okruženje:
```bash
source .venv/bin/activate
```
3. Instalirati potrebne Python pakete:
```python
pip3 install -r requirements.txt
```
4. Pokrenuti aplikaciju uz opcionalno postavljanje željenih vrata (default je 5000):
```bash
python3 ./src/index.py (-p 1234)?
```
