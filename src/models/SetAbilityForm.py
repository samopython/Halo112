from flask_wtf import FlaskForm
from wtforms import SelectMultipleField, SubmitField, SelectField

class Ability(FlaskForm):
    AbilityName = SelectField('Ability: ', coerce=int)
    submit = SubmitField('Add')