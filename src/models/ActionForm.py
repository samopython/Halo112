from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, FileField, TextAreaField
from wtforms.validators import DataRequired


class ActionForm(FlaskForm):
    naziv = StringField('Naziv: ', validators=[DataRequired()])
    opis = TextAreaField('Opis: ', validators=[DataRequired()])
    slika = FileField('Slika: ', validators=[])
    submit = SubmitField('Stvori akciju')
