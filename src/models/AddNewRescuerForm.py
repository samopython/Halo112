from flask_wtf import FlaskForm
from wtforms import SelectMultipleField, SubmitField, SelectField

class Rescuer(FlaskForm):
    Rescuers = SelectField('Rescuer: ', coerce=int)
    submit = SubmitField('Add rescuer')