from sqlalchemy.orm import relation, relationship
from shared import db

class Korisnik(db.Model):
    __tablename__ = 'korisnik'
    id = db.Column(db.Integer, primary_key=True)
    ime = db.Column(db.String(50), nullable=False)
    prezime = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(50), nullable=False)
    brojMob = db.Column(db.String(25), nullable=False)
    korisnickoIme = db.Column(db.String(30), nullable=False, unique=True)
    fotografija = db.Column(db.String(255), nullable=False)
    lozinka = db.Column(db.String(50), nullable=False)
    uloga = db.Column(db.String(25), nullable=False)
    obraden = db.Column(db.Boolean, nullable=False)

    vezaVoditeljStaniceStanica = relationship("Stanica", backref=db.backref("korisnik", uselist=False)) # One to one - razmisliti da li vezu stanice sa korisnikom ili spasiocem
    vezaSpasioc = relationship("Spasioci", backref=db.backref("korisnik", uselist=False))  # one to one

    def __init__(self, ime, prezime, email, brojMob, korisnickoIme, fotografija, lozinka, uloga, obraden):
        self.ime = ime
        self.prezime = prezime
        self.email = email
        self.brojMob = brojMob
        self.korisnickoIme = korisnickoIme
        self.fotografija = fotografija
        self.lozinka = lozinka
        self.uloga = uloga
        self.obraden = obraden


class Stanica(db.Model):
    __tablename__ = 'stanica'

    sifStanice = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idVoditelja = db.Column(db.Integer, db.ForeignKey('korisnik.id'))
    vezaSpasioci = relationship("Spasioci", backref=db.backref("stanica"))  # One to many
    
    imeStanice = db.Column(db.String(50), nullable=False)
    lokacija = db.Column(db.String(50), nullable=False)
    tipStanice = db.Column(db.String(50), nullable=False)

    def __init__(self, idVoditelja, imeStanice, lokacija, tipStanice):
        self.idVoditelja = idVoditelja
        self.imeStanice = imeStanice
        self.lokacija = lokacija
        self.tipStanice = tipStanice

    def __repr__(self):
        return '<Stanica %r>' % self.imeStanice


class Spasioci(db.Model):
    __tablename__ = 'spasioci'

    idSpasioc = db.Column(db.Integer, db.ForeignKey("korisnik.id"), nullable=False, primary_key=True)
    sifStanice = db.Column(db.Integer,  db.ForeignKey('stanica.sifStanice'))
    dostupnost = db.Column(db.Boolean, nullable=False, default=True)
    sposobnost = db.Column(db.String(50), default=None)
    idAkcije = db.Column(db.Integer, db.ForeignKey("akcija.id"))
    latitude = db.Column(db.Float, nullable=True)
    longitude = db.Column(db.Float, nullable=True)

    def __init__(self, idSpasioc, sifStanice, dostupnost, latitude, longitude):
        self.idSpasioc = idSpasioc
        self.sifStanice = sifStanice
        self.dostupnost = dostupnost
        self.latitude = latitude
        self.longitude = longitude

    def __repr__(self):
        return '<Spasioc %r>' % self.idSpasioc

class Akcija(db.Model):
    __tablename__ = 'akcija'

    id = db.Column(db.Integer, primary_key=True)
    naziv = db.Column(db.String(100), nullable=False)
    opis = db.Column(db.String(500), nullable=False)
    fotografija = db.Column(db.String(255), nullable=False)
    idDispatcher = db.Column(db.Integer,  db.ForeignKey('korisnik.id'))
    datum = db.Column(db.DateTime(), nullable=False)
    gotova = db.Column(db.Boolean(), nullable=False)

    def __init__(self, naziv, opis, fotografija, idDispatcher, datum, gotova = False):
        self.naziv = naziv
        self.opis = opis
        self.fotografija = fotografija
        self.idDispatcher = idDispatcher
        self.datum = datum
        self.gotova = gotova

class Zahtjev(db.Model):
    __tablename__ = 'zahtjev'

    id = db.Column(db.Integer, primary_key=True)
    idAkcija = db.Column(db.Integer,  db.ForeignKey('akcija.id'))
    hitnost = db.Column(db.String(50), nullable=False)
    spasioc = db.Column(db.Integer, db.ForeignKey("spasioci.idSpasioc"))

    def __init__(self, hitnost, idAkcija, spasioc):
        self.hitnost = hitnost
        self.idAkcija = idAkcija
        self.spasioc = spasioc

class Komentari(db.Model):
    __tablename__ = 'komentari'

    id = db.Column(db.Integer, primary_key=True)
    komentar = db.Column(db.String(150), nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    idAkcija = db.Column(db.Integer,  db.ForeignKey('akcija.id'))

    def __init__(self, komentar, latitude, longitude, idAkcije):
        self.komentar = komentar
        self.latitude = latitude
        self.longitude = longitude
        self.idAkcija = idAkcije

class Zadatak(db.Model):
    __tablename__ = 'zadatak'

    id = db.Column(db.Integer, primary_key=True)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    opis = db.Column(db.String(150))
    idAkcije = db.Column(db.Integer, db.ForeignKey('akcija.id'))
    idSpasioca = db.Column(db.Integer, db.ForeignKey('spasioci.idSpasioc'))

    def __init__(self, latitude, longitude, opis, idAkcije, idSpasioca):
        self.latitude = latitude
        self.longitude = longitude
        self.opis = opis
        self.idAkcije = idAkcije
        self.idSpasioca = idSpasioca