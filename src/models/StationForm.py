from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, SelectField
from wtforms.validators import EqualTo, DataRequired, Length
from wtforms_sqlalchemy.fields import QuerySelectField
from models.DBModels import Spasioci

class Station(FlaskForm):
    StationName = StringField('Station Name: ', validators=[DataRequired()])
    Location = StringField('Location: ', validators=[DataRequired()])
    StationType = SelectField('Station Type: ', choices=['Klinički bolnički centar', 'Policijska postaja', 'Vatrogasna postaja'])
    StationChief = SelectField('Station Chief: ', coerce=int)
    submit = SubmitField('Create Station')
