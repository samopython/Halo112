from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField
from wtforms.validators import DataRequired
from shared import sve_sposobnosti

class ZahtjevFormFirst(FlaskForm):
    sposobnost = SelectField('Osposobljenost: ', choices=sve_sposobnosti, validators=[DataRequired()])
    stanica = SelectField('Stanica: ', coerce=int)
    submit = SubmitField('Sljedeća forma')

class ZahtjevFormSecond(FlaskForm):
    hitnost = SelectField('Razina hitnosti: ', choices=['Niska', 'Srednja', 'Visoka'], validators=[DataRequired()])
    spasioc = SelectField('Spasioc: ', coerce=int)
    submit = SubmitField('Pošalji')
