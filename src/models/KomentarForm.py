from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class KomentarForm(FlaskForm):
    komentar = StringField('Komentar: ', validators=[DataRequired()])
    submit = SubmitField('Save')
