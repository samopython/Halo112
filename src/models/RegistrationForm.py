from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, FileField, IntegerField, SelectField
from wtforms.validators import EqualTo, DataRequired, Length, Email, Regexp


class RegisterForm(FlaskForm):
    name = StringField('Name: ', validators=[DataRequired()])
    surname = StringField('Surname: ', validators=[DataRequired()])
    username = StringField('Username: ',
                           validators=[DataRequired(), Length(4, 30, "Username must be between 4 and 30 characters."),
                                       Regexp(regex='^\w+$',
                                              message="Username can only contain Alphanumerical characters.")])
    role = SelectField('Role:', choices=['112 Dispatcher', 'Doctor', 'Firefighter', 'Policeman'])
    phoneNumber = IntegerField('Phone number: +385 ', validators=[DataRequired("Only numbers please.")])
    email = StringField('Email: ', validators=[DataRequired(), Email()])
    picture = FileField('Your photo: ', validators=[DataRequired()])
    password = PasswordField('Password: ',
                             validators=[DataRequired(),
                                         Length(6, 50, "Password must be between 6 and 50 characters.")])
    passwordCheck = PasswordField('Repeat password:',
                                  validators=[DataRequired(), EqualTo('password', message='Passwords must match.')])
    submit = SubmitField('Register')
