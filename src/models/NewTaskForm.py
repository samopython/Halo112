from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField, StringField
from wtforms.validators import DataRequired

class NewTaskForm(FlaskForm):
    spasioc = SelectField('Spasioc: ', coerce=int)
    opis = StringField('Opis zadatka : ', validators=[DataRequired()])
    submit = SubmitField('Dodaj')
