from typing import Optional
from flask_wtf import FlaskForm

from wtforms import StringField, SubmitField, PasswordField, FileField, IntegerField, SelectField
from wtforms.validators import EqualTo, DataRequired, Length

class UserEditForm(FlaskForm):
    ime = StringField('Name: ', validators=[DataRequired()])
    prezime = StringField('Surname: ', validators=[DataRequired()])
    korisnickoIme = StringField('Username: ', validators=[DataRequired()])
    uloga = SelectField('Role:', choices=[(0, '112 Dispatcher'), (1, 'Doctor'), (2, 'Firefighter'), (3, 'Policeman'), (4, 'Administrator')])  # Ovaj postavi kada se bude radio route,
    brojMob = StringField('Phone number: ', validators=[DataRequired('Wrong number')])
    email = StringField('Email: ', validators=[DataRequired()])
    fotografija = FileField('Your photo: ')
    stanica = SelectField('Stanica: ', coerce=int)  # TODO kako ako se uređuje dispatcher, on nema stanicu
    lozinka = PasswordField('Password: ', validators=[])
    lozinkaProvjera = PasswordField('Repeat password:',
                                  validators=[EqualTo('lozinka', message='Passwords must match')])
    submit = SubmitField('Save changes')

