from datetime import datetime
from inspect import getouterframes
from flask import Flask, session
from flask_session import Session
from flask_bootstrap import Bootstrap
from sqlalchemy.sql.elements import Null

from views.viewMap import viewMap
from views.views import views
from views.auth import auth
from shared import db
from models.DBModels import Korisnik, Spasioci, Stanica
from views.applications import applications
from views.users import users
from views.stations import stations
from views.actions import actions
from views.act_request import act_request

import os
from random import randint, random

app = Flask(__name__)

UPLOAD_FOLDER = 'src/pictures'

# Također se može koristiti baza u datotečnom obliku, umjesto :memory: staviti
# path do datoteke, npr /home/halo112/data.db
# Ukoliko se koristi postgres, treba se usmjeriti na odgovarajuću adresu (npr port 5433):
# DB_URI = os.getenv('DATABASE_URL', 'postgresql://postgres:bazepodataka@localhost:5433')

DB_URI = os.getenv('DATABASE_URL', 'sqlite:///:memory:')


# Heroku passa malo drugačiji prefix
if DB_URI.find("postgres") != -1 and DB_URI.find("postgresql") == -1:
   DB_URI = DB_URI.replace("postgres", "postgresql")

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SQLALCHEMY_DATABASE_URI'] = DB_URI  # insert URI of database
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['SECRET_KEY'] = 'C2HWGVoMGfNTBsrYQg8EcMrdTimkZfAb'
app.config["SESSION_PERMANENT"] = False
app.config['SESSION_TYPE'] = 'filesystem'
Session(app)
db.init_app(app)
bootstrap = Bootstrap(app)


@app.before_first_request
def before_first_request_func():
    uloge = ['Administrator', '112 Dispatcher', 'Doctor', 'Firefighter', 'Policeman']
    session.clear()
    db.drop_all()
    db.create_all()
    admin = Korisnik(
        korisnickoIme=f'admin',
        ime=f'Admin',
        prezime=f'Admininović',
        lozinka='admin',
        brojMob='099 999 9999',
        email=f'admin@halo112.com',
        fotografija='?',
        uloga='Administrator',
        obraden=True
    )
    db.session.add(admin)

    imena = ['Luka', 'Ivan', 'David', 'Marija', 'Ana', 'Petra']
    prezimena = ['Horvat', 'Kovačević', 'Babić', 'Marić', 'Jurić', 'Novak']
    for u_i, uloga in enumerate(uloge):
        for i in range(2):
            ime = imena[randint(0, len(imena)-1)]
            prezime = prezimena[randint(0, len(prezimena)-1)]
            user = Korisnik(
                korisnickoIme=f"{uloga.replace(' ','_')+str(i+1)}",
                ime=f'{ime}',
                prezime=f'{prezime}',
                lozinka='sifra123',
                brojMob='099 999 9999',
                email=f'{ime}{prezime}{i}{u_i}@halo112.com',
                fotografija='?',
                uloga=uloga,
                obraden=(i % 2 == 0)
            )
            db.session.add(user)
            db.session.commit()
            if user.obraden and user.uloga not in ['112 Dispatcher', 'Administrator']:
                spasioc = Spasioci(idSpasioc=user.id, sifStanice=None, dostupnost=True, latitude=round(0.08*random()+45.77, 4), longitude=round(0.25*random()+15.85, 4))
                db.session.add(spasioc)
    stanice = [('KBC Rebro', 'Kišpatićeva 12, 10000, Zagreb', 'Klinički bolnički centar', 6),
               ('JVP Zagreb Centar', 'Savska cesta 1/3, 10000, Zagreb', 'Vatrogasna postaja', 8),
               ('PPP Zagreb', 'Heinzelova 98, 10000, Zagreb', 'Policijska postaja', 10)]
    for ime, adresa, tip, voditelj in stanice:
        stanica = Stanica(imeStanice=ime, lokacija=adresa,
                          tipStanice=tip, idVoditelja=voditelj)
        db.session.add(stanica)
        spasioc = Spasioci.query.filter_by(idSpasioc=voditelj).first()
        spasioc.sifStanice = stanica.sifStanice


    db.session.commit()


app.register_blueprint(views, url_prefix='/')
app.register_blueprint(auth, url_prefix='/')
app.register_blueprint(applications, url_prefix='/')
app.register_blueprint(users, url_prefix='/')
app.register_blueprint(stations,url_prefix='/')
app.register_blueprint(actions, url_prefix='/')
app.register_blueprint(act_request, url_prefix='/')
app.register_blueprint(viewMap, url_prefix='/')

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Pokretanje Halo112 web servera.')
    parser.add_argument("-p", required=False, type=int, default=5000)
    args = parser.parse_args()

    app.run(debug=True, host='0.0.0.0', port=args.p)
