from flask_sqlalchemy import SQLAlchemy
from flask import Flask

UPLOAD_FOLDER = 'src/pictures'
ALLOWED_EXTENSIONS = {'png', 'jpg'}

#Hardkodirane sposobnosti po ulogama
sposobnosti_policajaca = [(1, 'Kretanje pješke'), (2, 'Vožnja motociklom'), (3, 'Vožnja autom'),
                            (4, 'Vožnja oklopnog vozila')]
sposobnosti_vatrogasaca = [(1, 'Vožnja autocisterne'), (2, 'Vožnja autoljestvi'),
                            (3, 'Vožnja zapovijednog vozila'), (4, 'Vožnja šumskog vozila')]
sposobnosti_doktora = [(1, 'Vožnja motociklom'), (2, 'Putnik u kolima hitne pomoći')]
sve_sposobnosti = [
'Kretanje pješke (Policeman)', 'Vožnja motociklom (Policeman)', 'Vožnja autom (Policeman)',
'Vožnja oklopnog vozila (Policeman)', 'Vožnja autocisterne (Firefighter)', 'Vožnja autoljestvi (Firefighter)',
'Vožnja zapovijednog vozila (Firefighter)', 'Vožnja šumskog vozila (Firefighter)',
'Vožnja motociklom (Doctor)', 'Putnik u kolima hitne pomoći (Doctor)']

db = SQLAlchemy()

app = Flask(__name__)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
