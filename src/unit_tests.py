from flask_testing import TestCase
import unittest
from index import app
from shared import db
from flask import session
from models.DBModels import Korisnik, Spasioci, Stanica
from random import randint

class Test(TestCase):

    def create_app(self):
        app.config['DEBUG'] = True
        app.config['TESTING'] = True
##        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        return app

    def setUp(self):
        uloge = ['Administrator', '112 Dispatcher', 'Doctor', 'Firefighter', 'Policeman']
        session.clear()
        db.drop_all()
        db.create_all()
        admin = Korisnik(
            korisnickoIme=f'admin',
            ime=f'Admin',
            prezime=f'Admininović',
            lozinka='admin',
            brojMob='099 999 9999',
            email=f'admin@halo112.com',
            fotografija='?',
            uloga='Administrator',
            obraden=True
        )
        db.session.add(admin)

        imena = ['Luka', 'Ivan', 'David', 'Marija', 'Ana', 'Petra']
        prezimena = ['Horvat', 'Kovačević', 'Babić', 'Marić', 'Jurić', 'Novak']
        for u_i, uloga in enumerate(uloge):
            for i in range(2):
                ime = imena[randint(0, len(imena)-1)]
                prezime = prezimena[randint(0, len(prezimena)-1)]
                user = Korisnik(
                    korisnickoIme=f"{uloga.replace(' ','_')+str(i+1)}",
                    ime=f'{ime}',
                    prezime=f'{prezime}',
                    lozinka='sifra123',
                    brojMob='099 999 9999',
                    email=f'{ime}{prezime}{i}{u_i}@halo112.com',
                    fotografija='?',
                    uloga=uloga,
                    obraden=True
                )
                db.session.add(user)
                db.session.commit()
                if user.obraden and user.uloga not in ['112 Dispatcher', 'Administrator']:
                    spasioc = Spasioci(idSpasioc=user.id, sifStanice=None, dostupnost=True)
                    db.session.add(spasioc)
        stanice = [('KBC Rebro', 'Kišpatićeva 12, 10000, Zagreb', 'Klinički bolnički centar', 6),
                   ('JVP Zagreb Centar', 'Savska cesta 1/3, 10000, Zagreb', 'Vatrogasna postaja', 8),
                   ('PPP Zagreb', 'Heinzelova 98, 10000, Zagreb', 'Policijska postaja', 10)]
        for ime, adresa, tip, voditelj in stanice:
            stanica = Stanica(imeStanice=ime, lokacija=adresa,
                              tipStanice=tip, idVoditelja=voditelj)
            db.session.add(stanica)
            spasioc = Spasioci.query.filter_by(idSpasioc=voditelj).first()
            spasioc.sifStanice = stanica.sifStanice


        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all() 


    def test_correct_login(self):
        with self.client:
            response = self.client.post(
                '/login',
                data=dict(username="admin", password="admin"),
                follow_redirects=True
            )
            self.assertIn(b'Create Station', response.data)


    def test_incorrect_login(self):
        with self.client:
            response = self.client.post('/login', data=dict(username="wrong", password="wrong"), follow_redirects=True)
            self.assertIn(b'Username or password invalid!', response.data)


    def test_signout(self):
        with self.client:
            self.client.post(
                '/login',
                data=dict(username="admin", password="admin"),
                follow_redirects=True
            )
            response = self.client.get('/signout', follow_redirects = True)
            self.assertIn(b'Login', response.data)
    


    def test_dispacher_newAction(self):
        with self.client:
            self.client.post(
                '/login',
                data=dict(username="112_Dispatcher1", password="sifra123"),
                follow_redirects=True
            )
            response = self.client.post('/actions/new', data=dict(naziv='Akcija1', opis='akcija akcija'), follow_redirects=True)
            self.assertIn(b'Akcija1', response.data)


if __name__ == '__main__':
    unittest.main()
