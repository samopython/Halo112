# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class TestCreateZahtjevMultiple():
  def setup_method(self, method):
    self.driver = webdriver.Chrome()
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_createZahtjevMultiple(self):
    self.driver.get("http://127.0.0.1:5000/")
    self.driver.set_window_size(1920, 1040)
    self.driver.find_element(By.LINK_TEXT, "Actions").click()
    self.driver.find_element(By.CSS_SELECTOR, "td:nth-child(8) > .btn").click()
    self.driver.find_element(By.ID, "sposobnost").click()
    dropdown = self.driver.find_element(By.ID, "sposobnost")
    dropdown.find_element(By.XPATH, "//option[. = 'Vožnja autocisterne (Firefighter)']").click()
    self.driver.find_element(By.ID, "stanica").click()
    dropdown = self.driver.find_element(By.ID, "stanica")
    dropdown.find_element(By.XPATH, "//option[. = 'JVP Zagreb Centar']").click()
    self.driver.find_element(By.ID, "submit").click()
    self.driver.find_element(By.ID, "hitnost").click()
    dropdown = self.driver.find_element(By.ID, "hitnost")
    dropdown.find_element(By.XPATH, "//option[. = 'Visoka']").click()
    self.driver.find_element(By.ID, "spasioc").click()
    self.driver.find_element(By.ID, "submit").click()
    self.driver.find_element(By.CSS_SELECTOR, "td:nth-child(8) > .btn").click()
    self.driver.find_element(By.ID, "sposobnost").click()
    dropdown = self.driver.find_element(By.ID, "sposobnost")
    dropdown.find_element(By.XPATH, "//option[. = 'Vožnja autocisterne (Firefighter)']").click()
    self.driver.find_element(By.ID, "stanica").click()
    dropdown = self.driver.find_element(By.ID, "stanica")
    dropdown.find_element(By.XPATH, "//option[. = 'JVP Zagreb Centar']").click()
    self.driver.find_element(By.ID, "submit").click()
    self.driver.find_element(By.ID, "hitnost").click()
    dropdown = self.driver.find_element(By.ID, "hitnost")
    dropdown.find_element(By.XPATH, "//option[. = 'Visoka']").click()
    self.driver.find_element(By.ID, "spasioc").click()
    self.driver.find_element(By.ID, "submit").click()
  
