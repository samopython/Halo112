from flask import render_template, Blueprint, redirect, url_for, session, request, flash
from models.DBModels import Korisnik, Stanica, Spasioci
from models.UserEditForm import UserEditForm
from shared import db, UPLOAD_FOLDER, ALLOWED_EXTENSIONS
from werkzeug.utils import secure_filename
from werkzeug.exceptions import BadRequestKeyError
import os
import copy

users = Blueprint('users', __name__)


@users.route('/users', methods=['GET', 'POST'])
def view_users():
    if session.get("username") is not None:
        users = Korisnik.query.filter_by(obraden=True)
        return render_template('RegisteredUsers.html.j2', users=users, session=session)
    else:
        return render_template('HomePage.html.j2', role=session["role"])

@users.route('/user/view/<string:id>', methods=['GET', 'POST'])  # Ruta za pregled podataka pojedinog korisnika i promjena tih podataka
def viewUserID(id):
    if session.get("username") == None:
        return redirect(url_for('auth.login'))

    # Ako je korisnik sesije admin, omogući uređivanje
    is_not_admin = str(session.get('role')) != 'Administrator'

    user = Korisnik.query.filter_by(id=id).first() # Dohvaćanje korisnika
    user_station = Spasioci.query.filter_by(idSpasioc=id).first()  # Dohvati ako je spasioc i dodijeljen stanici
    uloge = ['112 Dispatcher', 'Doctor', 'Firefighter', 'Policeman', 'Administrator']  # Hardkodirano i redoslijed se ne bi smio mijenjati

    brUloge = uloge.index(str(user.uloga))

    brStanice = -1
    if user_station != None and user_station.sifStanice != None:
        brStanice = int(user_station.sifStanice)

    form = UserEditForm(uloga=brUloge, ime=user.ime,
                    prezime=user.prezime, email=user.email,
                    fotografija=user.fotografija, korisnickoIme=user.korisnickoIme,
                        brojMob = str(user.brojMob), stanica=brStanice)


    stanice_select = [(c.sifStanice, c.imeStanice) for c in Stanica.query.all()]
    stanice_select.append((-1, 'Nema stanicu'))

    form.stanica.choices = stanice_select

    if form.validate_on_submit() and not is_not_admin:

        ### Ažuriranje tablice Korisnik
        updated_user = Korisnik.query.get(id)
        updated_spasioc = Spasioci.query.get(id)
        old_user = copy.deepcopy(updated_user)

        updated_user.uloga = uloge[int(form.uloga.data)]
        updated_user.ime = form.ime.data
        updated_user.prezime = form.prezime.data
        updated_user.email = form.email.data
        # updated_user.fotografija = form.fotografija.data
        updated_user.korisnickoIme = form.korisnickoIme.data
        updated_user.brojMob = form.brojMob.data

        # print(form.fotografija.data.filename)
        # print(updated_user.fotografija)

        # Provjera fotografije
        if form.fotografija.data.filename.strip() != "":
            # Spremi fotografiju i spremi filename fotografije
            #try:
            file = request.files['fotografija']
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(UPLOAD_FOLDER, filename))
                updated_user.fotografija = form.fotografija.data.filename
            else:
                form.fotografija.errors.append('.png or .jpg files only!')
                
            #except BadRequestKeyError as er:
                #pass
        print(updated_user.fotografija)

        if form.stanica.data == -1:
            form.stanica.data = None

        if form.lozinka.data != None:
            if len(str(form.lozinka.data).strip()) > 5:
                print("Lozinka je promijenjena")
                updated_user.lozinka = form.lozinka.data
            elif len(str(form.lozinka.data).strip()) == 0:
                print("Lozinka je nepromijenjena")
            else:
                print("Lozinka ima nedovoljno znakova")
                flash(message="Lozinka mora biti dugačka između 6 i 30 znakova! Ta promjena nije spremljena!", category="error")

        ### Ažuriranje tablice spasioc
        ### Spasioc ima === idSpasioc, obraden, sifStanice
        # Provjera ako dispečer u dispečer
        if old_user.uloga in ['112 Dispatcher', 'Administrator'] and updated_user.uloga in ['112 Dispatcher', 'Administrator']:
            pass
        # Provjera ako spasioc u spasioc
        elif old_user.uloga in ['Doctor', 'Firefighter', 'Policeman'] and updated_user.uloga in ['Doctor', 'Firefighter', 'Policeman']:
            if not Stanica.query.filter_by(idVoditelja=id).first(): # Ako je voditelj, ne može mijenjati stanicu
                updated_spasioc.sifStanice = form.stanica.data
            else:
                if updated_spasioc.sifStanice != form.stanica.data:
                    flash(message="Nije moguće promijeniti stanicu ako je osoba voditelj stanice! Ta promjena nije spremljena!", category="error")
        # Provjera ako spasioc u dispatcher
        elif old_user.uloga in ['Doctor', 'Firefighter', 'Policeman'] and updated_user.uloga in ['112 Dispatcher', 'Administrator']:
            # Provjera ako je voditelj
            stanicaVoditelj = Stanica.query.filter_by(idVoditelja=id).first()
            if stanicaVoditelj == None: # Nije voditelj, smije se prebaciti i mijenja ulogu
                spasioc_delete = Spasioci.query.filter_by(idSpasioc=id).first()
                db.session.delete(spasioc_delete)
            else: # Vraća staru ulogu i ne briše ga
                updated_user.uloga = old_user.uloga
                flash(message="Nije moguće promijeniti ulogu koja nije spasioc ako je osoba voditelj stanice! Promjena nije spremljena!", category="error")
        # Provjera da li iz dispečer u spasioc
        elif old_user.uloga in ['112 Dispatcher', 'Administrator'] and updated_user.uloga in ['Doctor', 'Firefighter', 'Policeman']:
            new_spasioc = Spasioci(idSpasioc=id ,sifStanice=form.stanica.data, dostupnost=True)
            db.session.add(new_spasioc)

        db.session.commit()
        return redirect(url_for('users.view_users'))

    return render_template('UserEditForm.html', form=form, user=user, session=session, is_not_admin=is_not_admin)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
