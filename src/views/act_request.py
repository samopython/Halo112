from flask import render_template, request, redirect, Blueprint, session, url_for, flash
from models.DBModels import Akcija, Spasioci, Zahtjev, Korisnik
from shared import db

act_request = Blueprint('act_request', __name__)

class DataDisplay:
    def __init__(self, idZahtjeva, idAkcija, hitnost, idSpasioc, naziv, opis, datum):
        self.idZahtjeva = idZahtjeva
        self.idAkcija= idAkcija
        self.hitnost = hitnost
        self.idSpasioc= idSpasioc
        self.naziv = naziv
        self.opis = opis
        self.datum = datum


@act_request.route('/act_requests', methods=['GET'])
def show_requests():
    # Preusmjeravanje za nedozvoljene uloge
    if session.get("username") == None:
        return redirect(url_for('auth.login'))
    if session.get("role") not in ["Doctor", "Firefighter", "Policeman"]:
        return redirect(url_for('views.home_page'))

    username = session.get("username")

    id = Korisnik.query.filter_by(korisnickoIme=username).first().id
    spasioc = Spasioci.query.filter_by(idSpasioc=id).first()

    if spasioc.idAkcije:
        # Ako je dio akcije ispiši odgovarajuću poruku
        return render_template('Act_Requests.html.j2', dio_akcije=True)

    requests = Zahtjev.query.filter_by(spasioc=int(id)).all()

    data_display = []

    for request in requests:
        #dodaj nazive akcija i opise u skup podataka
        action = Akcija.query.filter_by(id=request.idAkcija).first()
        date = action.datum.strftime("%Y-%m-%d %H:%M:%S")
        action_opis_cut = action.opis[0:50] + "..."
        data = DataDisplay(request.id, request.idAkcija, request.hitnost, request.spasioc, action.naziv, action_opis_cut, date)
        data_display.append(data)

    return render_template('Act_Requests.html.j2', data_display=data_display)


@act_request.route('/act_request/accept', methods=['POST'])
def accept_request():
    spasioc_username = session.get('username')
    idSpasioc = Korisnik.query.filter_by(korisnickoIme=spasioc_username).first().id
    spasioc = Spasioci.query.filter_by(idSpasioc=idSpasioc).first()

    idAkcija = int(request.form['accept'])
    idZahtjev = int(request.form['id_zahtjev'])

    # Provjera da li akcija postoji i da li je aktivna --- Ako u međuvremenu akcija završila, javlja se poruka o završetku akcije
    #                                                       inače se vraća na Home page i spasioc se povezuje sa akcijom
    if Akcija.query.filter_by(id=idAkcija).first() and not Akcija.query.filter_by(id=idAkcija).first().gotova:
        if 'accept' in request.form.keys():
            spasioc.idAkcije = idAkcija
            spasioc.dostupnost = False
            Zahtjev.query.filter_by(id=idZahtjev).delete()
            db.session.commit()
    else:
        flash(message="Akcija je u međuvremenu završila!", category="error")
        return redirect(url_for('act_request.show_requests'))

    return redirect(url_for('views.home_page'))
