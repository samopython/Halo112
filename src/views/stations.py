from flask import render_template, Blueprint, redirect, url_for, session, request, flash
from models.DBModels import Korisnik, Stanica, Spasioci
from models.UserEditForm import UserEditForm
from shared import db, UPLOAD_FOLDER, ALLOWED_EXTENSIONS, sposobnosti_policajaca, sposobnosti_vatrogasaca, sposobnosti_doktora
from models.AddNewRescuerForm import Rescuer
from models.SetAbilityForm import Ability

stations = Blueprint('stations', __name__)

@stations.route('/station/view/<string:id>', methods=['GET'])  
def viewStationID(id):

    if session.get("username") == None:
        return redirect(url_for('auth.login'))


    single_station = db.session.query(Stanica).filter_by(sifStanice=id).first()
    voditelj = db.session.query(Korisnik).filter_by(id=single_station.idVoditelja).first()
    spasioci = db.session.query(Spasioci).filter_by(sifStanice=id).all()
    return render_template('ViewSingleStation.html.j2',station=single_station, leader=voditelj, rescuers=spasioci)

@stations.route('/station/view/<string:id>/new-rescuer', methods=['GET', 'POST'])
def newRescuer(id):
    error = None
    if session.get("username") == None:
        return redirect(url_for('auth.login'))
    if request.method == 'POST':
        novi_spasioc = Spasioci.query.filter_by(idSpasioc=request.form.get('Rescuers')).first() #Odredivanje koji spasioc je odabran za pridruzivanje stanici
        novi_spasioc.sifStanice = id #Spasiocu se mijenja sifra stanice kojoj pripada, vise nije None
        db.session.commit()
        return redirect(url_for('stations.viewStationID', id=id))
    else:
        form = Rescuer()
        #Odredivanje svih spasioca koji ne pripadaju ni jednoj stanici, voditelj moze dodati nekog od njih
        spasioci = [(x.idSpasioc, y.korisnickoIme) for x, y in
                db.session.query(Spasioci, Korisnik).join(Korisnik, Spasioci.idSpasioc == Korisnik.id)
                  .filter(Spasioci.sifStanice == None).all()]
        if len(spasioci) < 1:
            error = 'Nema nedodijeljenih spasioca'
        form.Rescuers.choices = spasioci
        return render_template('AddNewRescuerForm.html.j2', form=form, error=error)

@stations.route(('/station/view/<string:id>/add-ability/<string:idSpasioc>/<string:uloga>'), methods=['GET', 'POST'])
def addAbility(id, idSpasioc, uloga):

    if session.get("username") == None:
        return redirect(url_for('auth.login'))
    if request.method == 'POST':
        spasioc = Spasioci.query.filter_by(idSpasioc=idSpasioc).first()

        #Pretvorba oznake sposobnosti iz forme u ime sposobnosti, ovisno o ulozi
        #TODO Organizirat drugacije tablicu DodatnaSposobnost (mora nekako biti ukljucena i uloga)
        if uloga == 'Doctor':
            oznaka_sposobnosti = sposobnosti_doktora[int(request.form.get('AbilityName')) - 1][1]
        elif uloga == 'Policeman':
            oznaka_sposobnosti = sposobnosti_policajaca[int(request.form.get('AbilityName')) - 1][1]
        elif uloga == 'Firefighter':
            oznaka_sposobnosti = sposobnosti_vatrogasaca[int(request.form.get('AbilityName')) - 1][1]
        else:
            return redirect(url_for('stations.viewStationID', id=id))

        spasioc.sposobnost = oznaka_sposobnosti  #Promjena sposobnosti spasioca iz None u novoodredenu sposobnost
        # sposobnost = DodatnaSposobnost(idSpasioca=spasioc.id, sposobnost=oznaka_sposobnosti)    #Nova sposobnost sprema se u bazu
        # db.session.add(sposobnost)
        db.session.commit()
        return redirect(url_for('stations.viewStationID', id=id))
    else:
        form = Ability()

        #Odabir opcija u padajucem izborniku ovisno o ulozi spasioca
        if uloga == 'Doctor':
            form.AbilityName.choices = sposobnosti_doktora
        elif uloga == 'Policeman':
            form.AbilityName.choices = sposobnosti_policajaca
        elif uloga == 'Firefighter':
            form.AbilityName.choices = sposobnosti_vatrogasaca
        else:
            return redirect(url_for('stations.viewStationID', id=id))
        return render_template('SetAbilityForm.hml.j2', form=form)
