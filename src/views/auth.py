from flask import Blueprint, render_template, session, redirect, url_for, request

from models.LoginForm import LoginForm
from models.RegistrationForm import RegisterForm
from models.DBModels import Korisnik, Stanica
from shared import db
from werkzeug.utils import secure_filename
from shared import ALLOWED_EXTENSIONS,UPLOAD_FOLDER
import os
auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    form = LoginForm()
    if request.method == 'POST':
        if Korisnik.query.filter_by(korisnickoIme=form.username.data, lozinka=form.password.data).first() is not None:
            data = Korisnik.query.filter_by(korisnickoIme=form.username.data, lozinka=form.password.data).first()
            if data.obraden:
                session["username"] = form.username.data
                session["role"] = data.uloga

                # dodatni parametar za voditelja stanice
                idStanice = db.session.query(Stanica).filter_by(idVoditelja=data.id).first()
                if idStanice:
                    print(idStanice.sifStanice)
                    session["idStanice"] = idStanice.sifStanice
                    
                return redirect(url_for('views.home_page'))
            else:
                error = "Your account hasn't been validated!"
        else:
            error = "Username or password invalid!"
    elif session.get("username") is not None:
        return redirect(url_for('views.home_page'))
    return render_template('LoginForm.html.j2', form=form, error=error, session=session)

@auth.route('/register', methods=['GET', 'POST'])  # Register route
def register():
    form = RegisterForm()
    if request.method == 'POST' and form.validate():  # If form was successful check if user already exists
        if Korisnik.query.filter_by(email=form.email.data).first() is not None:
            form.email.errors.append("Email already in use!")
        elif Korisnik.query.filter_by(korisnickoIme=form.username.data).first() is not None:
            form.username.errors.append('Username already in use!')
        else:
            entry = Korisnik(form.name.data, form.surname.data, form.email.data,
                             form.phoneNumber.data, form.username.data, form.picture.data.filename,
                             form.password.data, form.role.data, False)
            file = request.files['picture']
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(UPLOAD_FOLDER, filename))
                db.session.add(entry)
                db.session.commit()
                return redirect(url_for('auth.login'))
            else:
                form.picture.errors.append('.png or .jpg files only!')
    elif session.get("username") is not None:
        return redirect(url_for('views.home_page'))

    return render_template('RegisterForm.html.j2', form=form, session=session)



@auth.route('/signout', methods=['GET'])
def signout():
    session.clear()
    return redirect(url_for('views.home_page'))


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
