from random import random

from flask import render_template, request, redirect, Blueprint, session, url_for
from models.DBModels import Korisnik, Spasioci
from shared import db

applications = Blueprint('applications', __name__)


@applications.route('/applications', methods=['GET', 'POST'])
def view_applications():
    print(session.get("role"))
    if session.get("role") != "Administrator":
        return redirect(url_for('views.home_page'))
    users = Korisnik.query.filter_by(obraden=False)
    return render_template('UserApplications.html.j2', users=users, session=session)


@applications.route('/handle_application', methods=['POST'])
def handle_application():
    if session.get("role") != "Administrator":
        return redirect(url_for('views.home_page'))
    if 'accept' in request.form.keys():
        korisnickoIme = request.form['accept']
        user = Korisnik.query.filter_by(korisnickoIme=korisnickoIme).first()
        user.obraden = True
        if user.uloga not in ['112 Dispatcher', 'Administrator']:
            spasioc = Spasioci(idSpasioc=user.id, sifStanice=None, dostupnost=True, latitude=round(random()+45, 4), longitude=round(random()+15, 4))
            db.session.add(spasioc)
        db.session.commit()
    else:
        korisnickoIme = request.form['reject']
        Korisnik.query.filter_by(korisnickoIme=korisnickoIme).delete()
    db.session.commit()
    return redirect('/applications')
