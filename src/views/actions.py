from flask import render_template, request, redirect, Blueprint, session, url_for
from models.DBModels import Akcija, Korisnik, Spasioci, Stanica, Zahtjev
from shared import db, allowed_file, UPLOAD_FOLDER
from werkzeug.utils import secure_filename
from models.ActionForm import ActionForm
from models.ZahtjevForm import ZahtjevFormFirst, ZahtjevFormSecond

from datetime import datetime
import os

actions = Blueprint('actions', __name__)

@actions.route('/actions', methods=['GET'])
def view_actions():
    if session.get("role") not in ["112 Dispatcher", "Administrator"]:
        return redirect(url_for('views.home_page'))
    if session.get("role") == "Administrator":
        akcije = Akcija.query.all()
    else:
        idDispatcher = Korisnik.query.filter_by(korisnickoIme=session.get("username")).first().id
        akcije = Akcija.query.filter_by(idDispatcher=idDispatcher, gotova=False)
    return render_template('Actions.html.j2', actions=akcije, session=session)

@actions.route('/actions/view/<string:id>', methods=['GET', 'POST'])
def view_action(id):
    # TODO: implementacija prikaza akcije
    return redirect(url_for('views.home_page'))

@actions.route('/actions/spasioci/<string:id>', methods=['GET'])
def view_spasioci(id):
    if session.get("role") not in ["112 Dispatcher", "Administrator"]:
        return redirect(url_for('views.home_page'))
    spasioci = db.session.query(Spasioci, Korisnik).join(Korisnik).filter(Korisnik.obraden==True).filter(Spasioci.idAkcije==id).all()
    return render_template('ActionsSpasioci.html.j2', spasioci=spasioci, session=session)

@actions.route('/actions/spasioci/<string:id>/<string:id2>', methods=['GET', 'POST'])
def ukloni_korisnika(id, id2):
    spasioc = Spasioci.query.filter_by(idAkcije=id, idSpasioc=id2).first()
    spasioc.idAkcije = None
    spasioc.dostupnost = True
    db.session.commit()
    return redirect('/actions/spasioci/'+id)

@actions.route('/actions/<string:id>/newZahtjev', methods=['GET', 'POST'])
def create_zahtjev(id):
    first = (request.method == 'POST' and 'sposobnost' in request.form) or (request.method == 'GET')
    if session.get("role") != "112 Dispatcher" and session.get("role") != "Administrator":
        return redirect(url_for('views.home_page'))
    error = None
    if first:
        form = ZahtjevFormFirst()
        stanice = [(x.sifStanice, x.imeStanice) for x in Stanica.query.all()]
        stanice.insert(0, (-1, "Sve stanice"))
        form.stanica.choices = stanice

        if request.method == 'POST' and form.validate_on_submit():
            sposobnost = " ".join(form.sposobnost.data.split(" ")[:-1])
            spasioci = db.session.query(Spasioci, Korisnik).join(Korisnik, Spasioci.idSpasioc == Korisnik.id) \
                         .filter(Spasioci.dostupnost == True).filter(Spasioci.sposobnost == sposobnost)
            if int(form.stanica.data) != -1:
                spasioci = spasioci.filter(Spasioci.sifStanice == form.stanica.data)
            spasioci = [(x.idSpasioc, y.korisnickoIme) for x, y in spasioci.all()]
            form = ZahtjevFormSecond()
            form.spasioc.choices = spasioci
            if len(spasioci) == 0: error = "Spasioci odabranih karakteristika nisu dostupni!"
            return render_template('ZahtjevFormSecond.html.j2', form=form, error=error)
        return render_template('ZahtjevFormFirst.html.j2', form=form, error=error)
    else:
        form = ZahtjevFormSecond()
        zahtjev = Zahtjev(form.hitnost.data, id, form.spasioc.data)
        db.session.add(zahtjev)
        db.session.commit()
        return redirect('/actions')

@actions.route('/actions/new', methods=['GET', 'POST'])
def create_actions():
    if session.get("role") != "112 Dispatcher" and session.get("role") != "Administrator":
        return redirect(url_for('views.home_page'))

    form = ActionForm()
    error = None
    if request.method == 'POST' and form.validate():
        idDispatcher = Korisnik.query.filter_by(korisnickoIme=session.get("username")).first().id
        action = Akcija(form.naziv.data, form.opis.data, form.slika.data.filename, idDispatcher, datetime.now())
        file = request.files['slika']
        if file:
            if allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(UPLOAD_FOLDER, filename))
                db.session.add(action)
                db.session.commit()
                return redirect('/actions')
            else:
                error = '.png or .jpg files only!'
        else:
            db.session.add(action)
            db.session.commit()
            return redirect('/actions')
    return render_template('ActionForm.html.j2', form=form, error=error)

@actions.route('/actions/finish/<string:id>', methods=['GET', 'POST'])
def finish_action(id):
    akcija = Akcija.query.filter_by(id=id).first()
    spasioci = Spasioci.query.filter_by(idAkcije=id).all()
    Zahtjev.query.filter_by(idAkcija=id).delete()
    for spasioc in spasioci:
        spasioc.idAkcije = None
        spasioc.dostupnost = True
    akcija.gotova = True
    db.session.commit()
    return redirect('/actions')
