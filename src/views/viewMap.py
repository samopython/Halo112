import polyline
import requests
from branca.element import MacroElement
from flask import Blueprint, render_template, session, url_for, request
from jinja2 import Template
from werkzeug.utils import redirect
from folium.plugins import MousePosition
from models.KomentarForm import KomentarForm
from models.NewTaskForm import NewTaskForm


from models.DBModels import Spasioci, Korisnik, Komentari, Akcija, Zadatak
import folium
from shared import db

viewMap = Blueprint('view', __name__)


class LatLngPopup(MacroElement):
    """
    When one clicks on a Map that contains a LatLngPopup,
    a popup is shown that displays the latitude and longitude of the pointer.

    """
    _template = Template(u"""
            {% macro script(this, kwargs) %}
                var {{this.get_name()}} = L.popup();
                function latLngPop(e) {
                data =  "/kartaSvih/" + e.latlng.lat.toFixed(4) + "," + e.latlng.lng.toFixed(4);
                    {{this.get_name()}}
                        .setLatLng(e.latlng)
                        .setContent('<iframe id="iframe" src="'+data+'"></iframe>')
                        .openOn({{this._parent.get_name()}})
                    }
                {{this._parent.get_name()}}.on('click', latLngPop);

            {% endmacro %}
            """)  # noqa

    def __init__(self):
        super(LatLngPopup, self).__init__()
        self._name = 'LatLngPopup'


@viewMap.route('/kartaAkcije/<string:idAkcije>', methods=['GET'])
def viewactionmap(idAkcije):
    #pristup mapi preko detalja akcije, svaka akcija ima svoju mapu na kojoj su prikazani pripadajući zadatci
    #ovakav pristup mapi je namijenjen za dispečera i admina
    error = None
    if session.get("role") is None:
        return redirect(url_for('auth.login'))
    aktivne_akcije = db.session.query(Akcija).filter_by(id=idAkcije).first()
    if aktivne_akcije is None:
        error = 'Ne postoji trazena akcija, stoga za nju nema prikaza mape'
    elif session.get("role") == 'Spasioc':
        #spasioci imaju zaseban pristup mapi
        return redirect(url_for('viewMap.viewmap'))
    #prikazujemo sve spasioce i sve komentare zbog voronojevog dijagrama
    #ako ce dispecer htjet pogledat kog da jos ukljuci u akciju npr.
    spasioci = db.session.query(Spasioci).filter_by(idAkcije=idAkcije)
    komentari = db.session.query(Komentari).filter_by(idAkcija=idAkcije)
    zadatci = db.session.query(Zadatak).filter_by(idAkcije=idAkcije)
    boulder_coords = [45.8130, 15.9771]
    my_map = folium.Map(boulder_coords, zoom_start=14)
    my_map.add_child(LatLngPopup())
    for spasioc in spasioci:
        korisnik = db.session.query(Korisnik).filter_by(id=spasioc.idSpasioc).first()
        color = "blue"
        if korisnik.uloga == "Doctor":
            color = "green"
        if korisnik.uloga == "Firefighter":
            color = "red"
        popup = korisnik.ime + " " + korisnik.prezime + " \n " + korisnik.uloga
        folium.Marker(location=[spasioc.latitude, spasioc.longitude], popup=popup,
                      icon=folium.Icon(color=color, icon="user", prefix='fa')).add_to(my_map)
    for komentar in komentari:
        iframe = folium.IFrame(komentar.komentar, width=200, height=50)
        popup = folium.Popup(iframe, min_width=500, max_width=500)
        folium.Marker(location=[komentar.latitude, komentar.longitude], popup=popup,
                      icon=folium.Icon(color="blue", icon="comments", prefix='fa')).add_to(my_map)
    for zad in zadatci:
        iframe = folium.IFrame(zad.opis, width=200, height=50)
        popup = folium.Popup(iframe)
        folium.Marker(location=[zad.latitude, zad.longitude], popup=popup,
                      icon=folium.Icon(color='blue', icon='tasks', prefix='fa')).add_to(my_map)
    # rute se prikazuju samo spasiocima jer je njima ta info bitna, a dispečer samo vidi na kojoj se lokaciji u kojem
    # trenu nalazi koji spasioc
    mapa = my_map._repr_html_()
    session["akcija"] = idAkcije
    return render_template('viewMap.html.j2', map=mapa, idAkcije=idAkcije, error=error)


@viewMap.route('/kartaSvih', methods=['GET'])
def viewmap():
    #pristup mapi preko navbara, namijenjen za spasioce (i admin moze pristupiti ovome)
    #spasioci imaju prikaz mape s lokacijama svih spasioca, a ako su ukljuceni u neku akciju, vide i zadatke za nju
    if session.get("role") is None:
        return redirect(url_for('auth.login'))
    spasioci = db.session.query(Spasioci).all()
    komentari = db.session.query(Komentari).all()
    zadatci = db.session.query(Zadatak).all()
    username = session.get("username")
    boulder_coords = [45.8130, 15.9771]
    link1 = "https://router.project-osrm.org/route/v1/driving/"
    link2 = "?geometries=polyline&alternatives=true&steps=true&generate_hints=true"
    trenutna_akcija = None
    spasiocUser = db.session.query(Korisnik).filter_by(korisnickoIme=username).first()
    spasiocUser = db.session.query(Spasioci).filter_by(idSpasioc=spasiocUser.id).first()
    postoji_zadatak = False
    if session.get("role") in ["Doctor", "Policeman", "Firefighter"]:
        trenutna_akcija = spasiocUser.idAkcije
        boulder_coords = [spasiocUser.latitude, spasiocUser.longitude]
        link1 += str(spasiocUser.longitude) + ',' + str(spasiocUser.latitude) + ';'
        for zad in zadatci:
            if trenutna_akcija == zad.idAkcije and zad.idSpasioca == spasiocUser.idSpasioc:
                finish1 = zad.longitude
                finish2 = zad.latitude
                link1 += str(finish1) + ',' + str(finish2)
                postoji_zadatak = True

    my_map = folium.Map(boulder_coords, zoom_start=14)
    if postoji_zadatak:
        link1 += link2
        response = requests.get(link1)
        response = response.json()
        if response['code'] == 'Ok':
            ruta = response['routes'][0]['geometry']
            ruta = polyline.decode(ruta)
            folium.PolyLine(ruta, weight=8, color='blue', opacity=0.6).add_to(my_map)
    if trenutna_akcija:
        print(trenutna_akcija)
        for zad in zadatci:
            if zad.idAkcije == trenutna_akcija:
                iframe = folium.IFrame(zad.opis, width=200, height=50)
                popup = folium.Popup(iframe)
                folium.Marker(location=[zad.latitude, zad.longitude], popup=popup,
                              icon=folium.Icon(color='blue', icon='tasks', prefix='fa')).add_to(my_map)
        for spasioc in spasioci:
            if spasioc.idAkcije == trenutna_akcija:
                korisnik = db.session.query(Korisnik).filter_by(id=spasioc.idSpasioc).first()
                color = "blue"
                if korisnik.uloga == "Doctor":
                    color = "green"
                if korisnik.uloga == "Firefighter":
                    color = "red"
                iframe = folium.IFrame(korisnik.ime + " " + korisnik.prezime, width=100, height=50)
                popup = folium.Popup(iframe)
                folium.Marker(location=[spasioc.latitude, spasioc.longitude], popup=popup,
                          icon=folium.Icon(color=color, icon="user", prefix='fa')).add_to(my_map)
        for komentar in komentari:
            print(komentar.idAkcija)
            if komentar.idAkcija == trenutna_akcija:
                iframe = folium.IFrame(komentar.komentar, width=200, height=50)
                popup = folium.Popup(iframe, min_width=500, max_width=500)
                folium.Marker(location=[komentar.latitude, komentar.longitude], popup=popup,
                            icon=folium.Icon(color="blue", icon="comments", prefix='fa')).add_to(my_map)
    elif session.get("role") in ["Doctor", "Policeman", "Firefighter"]:
        for spasioc in spasioci:
            if spasioc.idSpasioc == spasiocUser.idSpasioc:
                korisnik = db.session.query(Korisnik).filter_by(id=spasioc.idSpasioc).first()
                color = "blue"
                if korisnik.uloga == "Doctor":
                    color = "green"
                if korisnik.uloga == "Firefighter":
                    color = "red"
                iframe = folium.IFrame(korisnik.ime + " " + korisnik.prezime, width=100, height=50)
                popup = folium.Popup(iframe)
                folium.Marker(location=[spasioc.latitude, spasioc.longitude], popup=popup,
                          icon=folium.Icon(color=color, icon="user", prefix='fa')).add_to(my_map)
    else:
        for spasioc in spasioci:
            korisnik = db.session.query(Korisnik).filter_by(id=spasioc.idSpasioc).first()
            print(korisnik.uloga)
            color = "blue"
            if korisnik.uloga == "Doctor":
                color = "green"
            if korisnik.uloga == "Firefighter":
                color = "red"
            iframe = folium.IFrame(korisnik.ime + " " + korisnik.prezime, width=100, height=50)
            popup = folium.Popup(iframe)
            folium.Marker(location=[spasioc.latitude, spasioc.longitude], popup=popup,
                              icon=folium.Icon(color=color, icon="user", prefix='fa')).add_to(my_map)
    mapa = my_map._repr_html_()
    return render_template('viewMap.html.j2', map=mapa)


@viewMap.route('/kartaSvih/<string:latlon>', methods=['GET', 'POST'])
def komentar(latlon):
    akcija = session.get('akcija')
    form = KomentarForm()
    if session.get("role") is None:
        return redirect(url_for('auth.login'))
    if len(latlon) != 15:
        return redirect(url_for('views.home_page'))
    latlon = latlon.split(",")
    if request.method == 'POST':
        entry = Komentari(form.komentar.data, latlon[0], latlon[1], idAkcije=akcija)
        db.session.add(entry)
        db.session.commit()
        return render_template('dodanKomentar.html')
    return render_template('komentar.html.j2', form=form, idAkcije=akcija, lat=latlon[0], lon=latlon[1])


@viewMap.route('/kartaAkcije/<string:idAkcije>/dodajNoviZadatak/<string:lat>/<string:lon>', methods=['GET', 'POST'])
def noviZadatak(idAkcije, lat, lon):
    if session.get("role") is None:
        return redirect(url_for('auth.login'))
    error = None

    if request.method == 'GET':
        form = NewTaskForm()
        #dispecer bira kojem spasiocu zadaje zadatak
        #ovdje je omoguceno da se ne mogu pojavit oni spasioci koji su ukljuceni u akciju, ali vec imaju zadan zadatak
        #tocnije, svaki spasioc moze u isto vrijeme dobiti samo jedan zadatak
        zadatak_akcije = db.session.query(Zadatak).filter(Zadatak.idAkcije == idAkcije).all()
        spasioci_sa_zadatcima = [x.idSpasioca for x in zadatak_akcije]
        spasioci = db.session.query(Spasioci, Korisnik).join(Korisnik, Spasioci.idSpasioc == Korisnik.id)\
            .filter(Spasioci.idAkcije == idAkcije)
        spasioci = [(x.idSpasioc, y.korisnickoIme) for x, y in spasioci.all()]
        spasioci_viska = list()
        for x, y in spasioci:
            if x in spasioci_sa_zadatcima:
                spasioci_viska.append(x)
        spasioci = [(x, y) for x, y in spasioci if x not in spasioci_viska]
        if len(spasioci) < 1:
            error = 'Za odabranu akciju trenutno ne postoji ni jedan spasioc na raspolaganju'
        form.spasioc.choices = spasioci
        return render_template('NewTaskForm.html.j2', form=form, error=error)
    elif request.method == 'POST':
        spasioc = request.form.get('spasioc')
        opis = request.form.get('opis')
        zadatak = Zadatak(lat, lon, opis, idAkcije, spasioc)
        db.session.add(zadatak)
        db.session.commit()
        return render_template('dodanZadatak.html')
