from flask import Blueprint, render_template, redirect, url_for, session, request
from werkzeug.utils import secure_filename

import os
from models.RegistrationForm import RegisterForm
from models.DBModels import Korisnik
from models.DBModels import Spasioci
from models.DBModels import Stanica
from models.StationForm import Station
from flask_sqlalchemy import SQLAlchemy
from shared import ALLOWED_EXTENSIONS,UPLOAD_FOLDER, db

views = Blueprint('views', __name__)


@views.route('/', methods=['GET'])
def home_page():
    return render_template('HomePage.html.j2', role=session.get("role"))


@views.route('/createStation', methods=['GET', 'POST'])
def createStation():
    print(session.get("role"))
    if session.get("role") != "Administrator":
        return redirect(url_for("views.home_page"))
    error = None

    form = Station()
    spasioci = [(x.idSpasioc, y.korisnickoIme) for x, y in
            db.session.query(Spasioci, Korisnik).join(Korisnik, Spasioci.idSpasioc == Korisnik.id)
              .filter(Spasioci.sifStanice == None).all()]

    form.StationChief.choices = spasioci

    if request.method == 'POST' and form.validate():

        StationName = request.form.get('StationName')
        Location = request.form.get('Location')
        StationType = request.form.get('StationType')
        StationChief = request.form.get('StationChief')

        station = Stanica.query.filter_by(imeStanice=StationName, lokacija=Location, tipStanice=StationType).first()
        if station:
            error = 'Station already exists!'
        else:
            print(StationChief)
            new_station = Stanica(imeStanice=StationName, lokacija=Location, tipStanice=StationType, idVoditelja=StationChief)
            db.session.add(new_station)
            db.session.commit()
            spasioc = Spasioci.query.filter_by(idSpasioc=StationChief).first()
            spasioc.sifStanice = new_station.sifStanice
            db.session.commit()
        return redirect(url_for('views.createStation'))
    return render_template('StationForm.html.j2', form=form, error=error)


@views.route('/viewStations', methods=['GET'])
def viewStations():
    if session.get("role") != "Administrator":
        return redirect(url_for("views.home_page"))
    error = None

    stanice = [x for x in db.session.query(Stanica).all()]
    
    voditelji = []
    for elem in stanice:
        voditelji.append(db.session.query(Korisnik).filter_by(id=elem.idVoditelja).first())
    
    return render_template('ViewStations.html.j2', error=error, data=stanice, voditelji=voditelji)

